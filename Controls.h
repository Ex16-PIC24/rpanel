/*! \file Controls.h

\brief Manifest constants for controls

This file defines the ID numbers for all the controls
and widgets used in RPanel.
*/

//! Background window
#define ID_WINDOW1     	10

//! Set Keyer button
#define ID_BUTKEYR      51
//! Set DSP button
#define ID_BUTDSP       52
//! Select memories button
#define ID_BUTMEM       53
//! Set gains button
#define ID_BUTGAIN      54
//! Keying weight button
#define ID_BUTWEIG      55
//! Keying speed button
#define ID_BUTSPD       56
//! Store memory button
#define ID_BUTSTOR      57
//! Iambic Key button
#define ID_BUTIAMB		58
//! Return to home screen button(s)
#define ID_BUTOK        59

//! Toggle AGC button
#define ID_BUTAGC		61
//! Toggle Filter button
#define ID_BUTFIL		62
//! Key speed up button
#define ID_BUTUP2		63
//! Key Speed DOwn button
#define ID_BUTDN2		64
//! Daytime hours up button
#define ID_BUTUP3		65
//! Daytime hours down button
#define ID_BUTDN3		66
//! Daytime minutes up button
#define ID_BUTUP4		67
//! Daytime minutes down button
#define ID_BUTDN4		68
//! Nighttime hours up button
#define ID_BUTUP5		69
//! Nighttime hours down button
#define ID_BUTDN5		70
//! Nighttime minutes up button
#define ID_BUTUP6		71
//! Nighttime minutes down button
#define ID_BUTDN6		72
// Memory buttons -- **NOTE** Must be consecutive
//! Memory button 1
#define ID_MEM1			81
//! Memory button 2
#define ID_MEM2			82
//! Memory button 3
#define ID_MEM3			83
//! Memory button 4
#define ID_MEM4			84
//! Memory button 5
#define ID_MEM5			85

//! Static Text 1
#define ID_TEXT1		31
//! Static Text 2
#define ID_TEXT2		32
//! Static Text 3
#define ID_TEXT3		33
//! Static Text 4
#define ID_TEXT4		34
//! Static Text 5
#define ID_TEXT5		35
//! Static Text 6
#define ID_TEXT6		36
//! Static Text 7
#define ID_TEXT7		37
//! Static Text 8
#define ID_TEXT8		38

//! Setpoint text
#define ID_TEXTSP		91
//! Frequency display text
#define ID_TEXTFREQ		92
//! CW Speed text
#define ID_TEXTKSPD		93
//! Main tuning knob
#define ID_ROUNDDIAL	120
//! S Meter
#define ID_PROGRESSBAR1	121
