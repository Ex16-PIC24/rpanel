/*! \file stringSubs.c

\brief String subroutines

This file contains a couple of basic strlib routines,
reverse and itoa, which seem to be lacking from strlib.
The routines are verbatim from K&R.
*/
#include <string.h>

//! reverse - reverse a character string
void reverse( char *s )
{
  unsigned char c,i,j;

  for ( i = 0, j = strlen(s)-1; i < j; i++, j-- )
    {
      c = s[i];
      s[i] = s[j];
      s[j] = c;
    }
}

//! itoa - generate ASCII representation of an integer
void itoa( int n, char *s )
{
  int i, sign;

  if ((sign = n) < 0 )
    n = -n;
  i = 0;
  do {
    s[i++] = n % 10 + '0';
  } while ((n /= 10) > 0);
  if (sign < 0 )
    s[i++] = '-';
  s[i] = '\0';
  reverse( s );
}

