/*! \file configbits.c

\brief  configuration bits for the PIC24FJ128GA010

This file sets the configuration bits for the target processor
*/
#include "RPanel.h"

//! Configuration word 1
_CONFIG2(FNOSC_PRIPLL & POSCMOD_XT) // Primary XT OSC with PLL
//! Configuration word 2
_CONFIG1(JTAGEN_OFF & FWDTEN_OFF)   // JTAG off, watchdog timer off

