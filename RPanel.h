/*! \file RPanel.h
 \brief Include file for rig front panel application

This file includes other headers required by most of
the files in the application.

 */

#ifndef _RPANEL_H
#define _RPANEL_H

//! Oscillator frequency
#define SYSCLK 32000000 // 8MHz x 4PLL

////////////////////////////// INCLUDES //////////////////////////////
#include <p24Fxxxx.h>
#include "GenericTypeDefs.h"
#include "Graphics.h"
#include "EEPROM.h"
#include "TouchScreen.h"
#include <GOL.h>
//#include "Beep.h"
//#include "SideButtons.h"
//#include "rtcc.h"

// Functions defined within the project
void itoa( int, char * );
void formSetpoint( void );
void makeSchemes( void );
void makePage( int );
void formSetpoint( void );
void formFreq( void );
void toggleFil( void );
int toggleAGC( void );
void setMemButtons( void );

#endif

