/*! \file RPanel_main.c

    \brief UI for a CW Radio

    Program runs on an Explorer 16 board with a Graphics PICtail Plus.
    It expects a PIC24FJ128GA010 and a Microtips PICtail.

\dotfile "mainTransitions.dot" "Transitions between screens"

*/

#include "RPanel.h"
#include <string.h>
#include "Controls.h"


//! Color scheme for buttons
GOL_SCHEME*    alt3Scheme;       // alternative style scheme
//! Background window emphasized text color scheme
GOL_SCHEME*    alt4Scheme;       // alternative style scheme
//! Background window color scheme
GOL_SCHEME*    wndScheme;        // alternative style scheme
//! Background window (de-emphasized text) color scheme
GOL_SCHEME*    wnd2Scheme;       // alternative style scheme

//! Page number to display, 0=keep old page
int nNewPage;
//! Current CW speed setting
int nSetpoint;
//! Selected Scheme
int nScheme;
//! Selector for S meter value
int nSValue;
//! Is it OK to update S meter?
int nSOK;
//! Previous S meter value
int nSLastValue;
//! S meter direction
int nSUpDown;
//! S meter
PROGRESSBAR*   pSMeter;
// Current frequency
extern int nFreq;
// Currently active page
extern int nThisPage;
//! Mask to remember which memory buttons are pressed
/*! nMems is a bit mask.  Bits <4-0> may be set to represent
whether controls ID-MEM5 through ID_MEM1 should be pressed.
*/
int nMems;

//! main - Mainline for Rig Front Panel
/*!
\b Pseudocode:
\code
Initialize EEPROM
Inititlize graphics library
Initialize touch Screen
IF PB3 is pressed
    Calibrate touch screen
IF touch screen not calibrated ever
    Calibrate touch screen
Select scheme 0 (default)
IF PB4 is pressed
    Select scheme 1
IF PB5 is pressed
    Select scheme 2
IF PB6 is pressed
    Select scheme 3
Initialize application variables
WHILE 1
    if GOLDraw()
        Get touch screen message
        Process message
        IF nNewPage != 0
            makePage()
\endcode
*/
int main(void){
GOL_MSG msg;                    // GOL message structure to interact with GOL


    EEPROMInit();               // Initialize EEPROM
    //TickInit();               // Start tick counter    
    GOLInit();                  // Initialize graphics library and crete default style scheme for GOL
    //BeepInit();               // Initialize beeper
    TouchInit();                // Initialize touch screen
    //RTCCInit();               // Setup the RTCC
    //RTCCProcessEvents();

    // If S3 button on Explorer 16 board is pressed calibrate touch screen
    if(PORTDbits.RD6 == 0){
        TouchCalibration();
        TouchStoreCalibration();
    }

    // If it's a new board (EEPROM_VERSION byte is not programed) calibrate touch screen
    if(GRAPHICS_LIBRARY_VERSION != EEPROMReadWord(EEPROM_VERSION)){
        TouchCalibration();
        TouchStoreCalibration();
        EEPROMWriteWord(GRAPHICS_LIBRARY_VERSION,EEPROM_VERSION);
    }

    // Load touch screen calibration parameters from EEPROM
    TouchLoadCalibration();

    nScheme = 0;
    // If S4 button is pressed, select scheme 1
    if(PORTDbits.RD13 == 0){
        nScheme = 1;
    }

    // If S5 button is pressed, select scheme 2
    if(PORTAbits.RA7 == 0){
        nScheme = 2;
    }

    // If S6 button is pressed, select scheme 3
    if(PORTDbits.RD7 == 0){
        nScheme = 3;
    }

    nSetpoint = 24;         // Initial CW speed setting
    formSetpoint();         // Create CW speed string
    nFreq = 600;            // Set initial freq to 14.060
    formFreq();             // Create freq string
    makeSchemes();          // Build color schemes
    nNewPage = 1;           // Start at frequency selection page
    makePage(1);            // Build page controls


    TMR1 = 0;               // clear timer 1
    PR1 = 0x7270;           // interrupt every 250ms
    IFS0bits.T1IF = 0;      // clr interrupt flag
    IEC0bits.T1IE = 1;      // set interrupt enable bit
    T1CON = 0x8020;         // Fosc/4, 1:256 prescale, start TMR1
    TRISA = 0;              // PORTA all outputs
    LATA = 0x80;            // Turn on leftmost LED

    nSValue = 1;            // Initialize S calculation
    nSUpDown = 1;           // Initialize S calculation
    nSOK = 1;               // Is it OK to update S meter?
    toggleFil();            // Toggle filter state and
    toggleFil();            // AGC state to get to desired
    toggleAGC();            // startup position
    toggleAGC();

    while(1){
      if(GOLDraw()){             // Draw GOL objects
	// Drawing is done here, process messages
	TouchGetMsg(&msg);     // Get message from touch screen
	GOLMsg(&msg);          // Process message
	if ( nNewPage )
	  makePage(nNewPage);
      }
    } 

}

//! GOLMsgCallback - Graphics library message handler
/*!
This function is called whenever a widget receives a message.

\dotfile msgCallbackFlow.dot "Message callback flow"
In order to minimize the number of decisions that must be made
for each message, first the OK button (which appears on every
screen) is tested, and then a selection is made based on the
currently active screen.  Within each screen individual messages
are tested.

\dotfile msgCallbackFlow1.dot "Main screen message handling"
The main screen is perhaps the most involved.  Messages from the
dial involve updating the frequency displayed.  Messages from the
AGC and Filter buttons toggle through those states.  Messages from
the Keyer, DSP or Memories buttons cause a transition to those screens.

\dotfile msgCallbackFlow2.dot "Keyer screen message handling"
The (partially implemented) keyer screen allows the CW speed to be
incremented or decremented with up/down buttons.  The intent of
the weight and speed buttons is to allow the selection of which
value is to be adjusted with the up/down buttons.  At the current
time all it does is change the button state.  Probably it would
be best to display both weight and speed for adjustment as there
is plenty of screen real estate.

\dotfile msgCallbackFlow4.dot "Memories screen message handling"
The memories screen displays buttons for the 5 memories.  When a
memory button is pushed, that button is toggled on and the others
toggled off.  The frequency represented by that button is moved
to the active frequency.  The intent of the store button is to 
cause pressing a frequency button to store that frequency.

*/
WORD GOLMsgCallback(WORD objMsg, OBJ_HEADER* pObj, GOL_MSG* pMsg)
{
    int nID;
    //int nState;
    OBJ_HEADER *pOtherObj;

    nID = GetObjID(pObj);
    if ( nID == ID_BUTOK )      // OK always returns to home screen
      nNewPage = 1;
    else
      switch ( nThisPage )    // Select page first to reduce the number
        {                       // of comparisons each loop
        case 2:         // Keyer page =========================================
	  switch ( nID )
            {
            case ID_BUTUP2:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  nSetpoint++;
		  if ( nSetpoint > 45 ) nSetpoint = 45;
		  formSetpoint();
		  pOtherObj = GOLFindObject( ID_TEXTKSPD );
		  SetState(pOtherObj,ST_DRAW); 
		  SetState(pObj,BTN_DRAW); 
                }
	      break;
            case ID_BUTDN2:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  nSetpoint--;
		  if ( nSetpoint < 13 ) nSetpoint = 13;
		  formSetpoint();
		  pOtherObj = GOLFindObject( ID_TEXTKSPD );
		  SetState(pOtherObj,ST_DRAW); 
		  SetState(pObj,BTN_DRAW); 
		}
	      break;
            case ID_BUTSPD:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  pOtherObj = GOLFindObject( ID_BUTWEIG );
		  SetState(pObj,BTN_PRESSED|BTN_DRAW); 
		  ClrState(pOtherObj,BTN_PRESSED); 
		  SetState(pOtherObj,BTN_DRAW); 
                }
	      break;
            case ID_BUTWEIG:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  pOtherObj = GOLFindObject( ID_BUTSPD );
		  SetState(pObj,BTN_PRESSED|BTN_DRAW);
		  SetState(pOtherObj,BTN_DRAW); 
		  ClrState(pOtherObj,BTN_PRESSED); 
                }
	      break;
            default:
	      break;
            }

	  break;
        case 3:         // DSP page ===========================================
	  break;
        case 4:         // Memory setting page ================================
	  switch ( nID )
            {
            case ID_MEM1:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  nMems = 0x01;
		  nFreq = 600;
		  setMemButtons();
                }
	      break;
            case ID_MEM2:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  nMems = 0x02;
		  nFreq = 580;
		  setMemButtons();
                }
	      break;
            case ID_MEM3:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  nMems = 0x04;
		  nFreq = 700;
		  setMemButtons();
                }
	      break;
            case ID_MEM4:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  nMems = 0x08;
		  nFreq = 400;
		  setMemButtons();
                }
	      break;
            case ID_MEM5:
	      if ( objMsg == BTN_MSG_PRESSED )
                {
		  nMems = 0x10;
		  nFreq = 200;
		  setMemButtons();
                }
	      break;
            }
	  break;
        default:        // Home page ==========================================
	  switch ( nID )
            {
            case ID_ROUNDDIAL:
	      nFreq = RdiaGetVal((ROUNDDIAL*)pObj);
	      if ( nFreq < 0 ) nFreq = 0;
	      if ( nFreq > 3500 ) nFreq = 3500;
	      formFreq();
	      pOtherObj = GOLFindObject( ID_TEXTFREQ );
	      SetState(pOtherObj,ST_DRAW); 
	      break;
            case ID_BUTKEYR:    // Select keyer setting page
	      nSOK = 0;
	      nNewPage = 2;
	      break;
            case ID_BUTDSP:     // Select DSP setting page
	      nSOK = 0;
	      nNewPage = 3;
	      break;
            case ID_BUTMEM:     // Select memories page
	      nSOK = 0;
	      nNewPage = 4;
	      break;
            case ID_BUTAGC:
	      if ( objMsg == BTN_MSG_RELEASED )
		{
		  if ( toggleAGC() )
		    pObj->state = DRAW|BTN_PRESSED;
		  else
		    pObj->state = DRAW;
		}
	      else
		pObj->state = DRAW|BTN_PRESSED;
	      break;
            case ID_BUTFIL:
	      if ( objMsg == BTN_MSG_PRESSED )
		{
		  toggleFil();
		  pObj->state = BTN_DRAW;
		}
	      break;
            default:
	      break;
            }
        }

    return 1;
}

//! GOLDrawCallback - Special drawing function handler
/*! GOLDrawCallback is currently only a placeholder.  It simply
returns a 1.
*/
WORD GOLDrawCallback()
{
  return 1;
}

//! Array of values for S meter
static const int nSRead[] = { 
  0, 0, 0,20,30,49,46,25,10, 0,
  0,10,15,20,12, 8, 0, 0, 0, 0,
  31,49,45,26, 8, 4, 1, 0, 0, 0,
  0, 0, 0,45,49,31, 0, 0, 0, 0,
  10,21,32,43,49,15, 0, 0, 0, 0
};

//! _T1Interrupt - Timer 1 Interrupt Service Routine
/*!
  This function is called multiple times per second.  Each time it
  is called it retrieves a new value from the nSRead array and sends
  it to the S Meter control.  It also toggles the left two LEDs.
*/
void __attribute__((interrupt, no_auto_psv)) _T1Interrupt(void)
{
  IFS0bits.T1IF = 0;      // clear interrupt flag
    
  LATA ^= 0xc0;           //Toggle LED's

  nSValue += nSUpDown;

  if ( nSValue < 0 )
    {
      nSValue = 0;
      nSUpDown = 1;
    }
  if ( nSValue >49 )
    {
      nSValue = 49;
      nSUpDown = -1;
    }

  if ( nSOK )
    {
      PbSetPos( pSMeter, nSRead[nSValue] );
      SetState( pSMeter, PB_DRAW );
    }

}


