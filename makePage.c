/*! \file makePage.c

    \brief Paints the pages

This file is responsible for building the controls
that make up the various screens in the application.

*/

#include "RPanel.h"
#include "Controls.h"
#include "colors.h"
#include <string.h>

// Color scheme for buttons
extern GOL_SCHEME*    alt3Scheme;        // alternative style scheme
// Background window bright yellow text color scheme
extern GOL_SCHEME*    alt4Scheme;        // alternative style scheme
// Background window color scheme
extern GOL_SCHEME*    wndScheme;         // alternative style scheme
// Background window (de-emphasized text) color scheme
extern GOL_SCHEME*    wnd2Scheme;        // alternative style scheme
//! Main frequency control dial
ROUNDDIAL *pDial;

// Page number to display, 0=keep old page
extern int nNewPage;
// Currently active setpoint
extern int nSetpoint;
// OK to update S Meter
extern int nSOK;
// Mask for memory buttons
extern int nMems;
// S Meter
extern PROGRESSBAR*   pSMeter;
//! Currently displayed page
int nThisPage;
//! Current filter setting
/*!
\li 0 = Fil
\li 1 = Wid
\li 2 = Med
\li 3 = Nar */
int nFiltState;
//! String representation of filter setting
char szFilt[8];
//! Current AGC setting
/*!
\li 0 = AGC
\li 1 = agcS
\li 2 = agcM
\li 3 = agcF */
int nAGC;
//! String representation of AGC setting
char szAGC[8];
//! Displayed setpoint as string
char szSetpoint[16];
//! Displayed frequency as string
char szFreq[16];
//! Base part of freq
/*! Currently the frequency is stored as an integer representing
units of 100 Hz.  The formFreq() function converts this to a
text string representing the desired operating frequency. */
int nFreq;


//! formFreq - form the frequency display string
/*! formFreq converts nFreq, which represents the hundreds
of kilohertz above 14 megahertz, and converts it to a string.
*/
void formFreq( void )
{
  char szWork[16];

  memset(szWork,0,sizeof(szWork));
  itoa(10000+nFreq,szWork);
  strcpy(szFreq,"14.");
  strcat(szFreq,&szWork[1]);
  szFreq[6]='.';
  szFreq[7]=szWork[4];
  szFreq[8]='\0';
  strcat(szFreq,"00");
}


//! formSetpoint - convert nSetpoint into properly formatted szSetpoint string
/*!
  The "Setpoint" is the desired CW speed.
  \b Pseudocode:
  \code
  Convert setpoint to string (itoa)
  \endcode
*/
void formSetpoint()
{
  itoa(nSetpoint,szSetpoint);
  //strcat(szSetpoint," WpM");
}


//! toggleFilt - cycle through filter states
/*!
  \dotfile Filter.dot "Filter text transitions"
*/
void toggleFil()
{
  nFiltState++;
  if ( nFiltState > 3 ) nFiltState = 0;
  switch ( nFiltState )
    {
    case 1:
      strcpy( szFilt, "Wid" );
      break;
    case 2:
      strcpy( szFilt, "Med" );
      break;
    case 3:
      strcpy( szFilt, "Nar" );
      break;
    default:
      strcpy( szFilt, "Fil" );
    }
}


//! toggleAGC - Cycle through AGC states
/*!
  \dotfile AGC.dot "AGC text transitions"
*/
int toggleAGC()
{
  nAGC++;
  if ( nAGC > 3 ) nAGC = 0;
  switch ( nAGC )
    {
    case 1:
      strcpy( szAGC, " agcS" );
      break;
    case 2:
      strcpy( szAGC, "agcM" );
      break;
    case 3:
      strcpy( szAGC, " agcF" );
      break;
    default:
      strcpy( szAGC, " AGC" );
    }
  return nAGC;
}


//! setMemButtons - set the pressed/not pressed state of the memory buttons
/*! The nMems mask contains a bit for each memory button.  Each bit is
  tested in turn, and if true, the corresponding button state is set to
  BTN_PRESSED. */
void setMemButtons( void )
{
  OBJ_HEADER *pObj;

  pObj = GOLFindObject( ID_MEM1 );
  if ( nMems & 0x01 )
    SetState( pObj, BTN_PRESSED|BTN_TOGGLE|BTN_DRAW);
  else
    SetState( pObj, BTN_TOGGLE|BTN_DRAW);

  pObj = GOLFindObject( ID_MEM2 );
  if ( nMems & 0x02 )
    SetState( pObj, BTN_PRESSED|BTN_TOGGLE|BTN_DRAW);
  else
    SetState( pObj, BTN_TOGGLE|BTN_DRAW);

  pObj = GOLFindObject( ID_MEM3 );
  if ( nMems & 0x04 )
    SetState( pObj, BTN_PRESSED|BTN_TOGGLE|BTN_DRAW);
  else
    SetState( pObj, BTN_TOGGLE|BTN_DRAW);

  pObj = GOLFindObject( ID_MEM4 );
  if ( nMems & 0x08 )
    SetState( pObj, BTN_PRESSED|BTN_TOGGLE|BTN_DRAW);
  else
    SetState( pObj, BTN_TOGGLE|BTN_DRAW);

  pObj = GOLFindObject( ID_MEM5 );
  if ( nMems & 0x10 )
    SetState( pObj, BTN_PRESSED|BTN_TOGGLE|BTN_DRAW);
  else
    SetState( pObj, BTN_TOGGLE|BTN_DRAW);

}


//! makePage - Layout each of the various screens
/*!
  \b Pseudocode:
  \code
  IF nNewPage == 0
  return
  Remember page number
  If filter or AGC strings null, toggle to generate text
  Free old list of objects
  Set current color to background color
  Clear the device
  CASE nPageNum
    1:
      Lay out buttons and text for home screen
        Set the dial value to current frequency
        Allow the S meter to be updated
    2:
      Lay out buttons and text for keyer screen
    3:
      Lay out buttons and text for DSP screen
    4:
        Lay out buttons and text for memories screen
        Set each button press state depending on nMems
  ENDCASE
  nNewPage = 0 (don't create new page)
  \endcode
*/
void makePage( int nPageNum )
{
  OBJ_HEADER* obj;
  //int x,xp1,xp2,yp1,yp2;
  //char szWork[16];

  //makeSchemes();
  if ( !nNewPage )
    return;

  nThisPage = nPageNum;

  if ( szFilt[0]=='\0' ) toggleFil();
  if ( szAGC[0]=='\0' ) toggleAGC();

  GOLFree();
  SetColor( wndScheme->CommonBkColor );
  ClearDevice();

  switch( nPageNum )
    {
    case 1:     // - - - - - - H o m e   W i n d o w - - - - - - 

      obj = (OBJ_HEADER *)WndCreate(ID_WINDOW1,             // ID
				    0,0,GetMaxX(),GetMaxY(),// dimension
				    WND_DRAW,               // will be dislayed after creation
				    //&mchpIcon,            // icon
				    NULL,                   // icon
				    NULL,                   // set text 
				    wndScheme);

      pDial = RdiaCreate(ID_ROUNDDIAL,            // ID
			 240,160,70,              // dimensions
			 RDIA_DRAW,               // draw after creation
			 1,                       // resolution
			 600,                     // initial value
			 3500,                    // maximum value
			 alt3Scheme);             // use alternative scheme
      RdiaSetVal(pDial, nFreq);

      pSMeter = PbCreate(ID_PROGRESSBAR1,// ID
			 160,5,310,30,            // dimension
			 PB_DRAW,                 // will be dislayed after creation
			 0,                       // position
			 50,                      // range
			 wnd2Scheme);             // use default scheme
      nSOK = 1;


      BtnCreate(ID_BUTKEYR, 10,180, 70,230,10,BTN_DRAW,           NULL,(XCHAR *)&szSetpoint[0],alt3Scheme);
      BtnCreate(ID_BUTFIL,  80,180,140,230,10,BTN_DRAW,           NULL,(XCHAR *)&szFilt[0],    alt3Scheme);
      BtnCreate(ID_BUTGAIN, 10,125, 70,175,10,BTN_DRAW,           NULL,"Gain",    alt3Scheme);
      BtnCreate(ID_BUTMEM,  80,125,140,175,10,BTN_DRAW,           NULL,"Mem",     alt3Scheme);
      BtnCreate(ID_BUTDSP,  10, 70, 70,120,10,BTN_DRAW|BTN_TOGGLE,NULL,"DSP",     alt3Scheme);
      if ( nAGC )
	BtnCreate(ID_BUTAGC,     80, 70,140,120,10,BTN_DRAW|BTN_PRESSED,NULL,(XCHAR *)szAGC,     alt3Scheme);
      else
	BtnCreate(ID_BUTAGC,     80, 70,140,120,10,BTN_DRAW,NULL,(XCHAR *)szAGC,     alt3Scheme);

      StCreate(ID_TEXT1,   160, 30,310, 60,DRAW|ST_CENTER_ALIGN," 1  3  5  7  9   +30   +60",wndScheme);
      StCreate(ID_TEXT4,    20, 30, 60, 55,DRAW|ST_CENTER_ALIGN,"NR",      wnd2Scheme);
      StCreate(ID_TEXT5,    70, 30,110, 55,DRAW|ST_CENTER_ALIGN,"MN",      wnd2Scheme);
      formFreq();
      StCreate(ID_TEXTFREQ, 10,  5,140, 30,DRAW|ST_CENTER_ALIGN,(XCHAR *)szFreq, alt4Scheme); //alt4

      break;

    case 2:     // - - - - - - Keyer   W i n d o w - - - - - - 

      obj = (OBJ_HEADER *)WndCreate(ID_WINDOW1,               // ID
				    0,0,GetMaxX(),GetMaxY(),  // dimension
				    WND_DRAW,                 // will be dislayed after creation
				    //&mchpIcon,                  // icon
				    NULL,                     // icon
				    NULL,                 // set text 
				    wndScheme);

      BtnCreate(ID_BUTOK,   164,180,290,215,10,BTN_DRAW,                       NULL,"OK",      alt3Scheme);
      BtnCreate(ID_BUTIAMB, 164,135,290,170,10,BTN_DRAW|BTN_TOGGLE,            NULL,"Iamb A",    alt3Scheme);
      BtnCreate(ID_BUTWEIG, 164, 90,290,125,10,BTN_DRAW|BTN_TOGGLE,            NULL,"Weight",   alt3Scheme);
      BtnCreate(ID_BUTSPD,  164, 45,290, 80,10,BTN_DRAW|BTN_PRESSED|BTN_TOGGLE,NULL,"Speed",     alt3Scheme);
      BtnCreate(ID_BUTUP2,   10,170, 70,220,10,BTN_DRAW,                       NULL,"Up",      alt3Scheme);
      BtnCreate(ID_BUTDN2,   90,170,150,220,10,BTN_DRAW,                       NULL,"Dn",      alt3Scheme);

      StCreate(ID_TEXTKSPD,20,70,145,100,ST_DRAW|ST_FRAME|ST_CENTER_ALIGN,(XCHAR *)&szSetpoint[0],alt4Scheme);

      break;

    case 3:     // - - - - - - DSP   W i n d o w - - - - - - 

      obj = (OBJ_HEADER *)WndCreate(ID_WINDOW1,              // ID
				    0,0,GetMaxX(),GetMaxY(), // dimension
				    WND_DRAW,                // will be dislayed after creation
				    //&mchpIcon,             // icon
				    NULL,                    // icon
				    NULL,                    // set text 
				    wndScheme);

      StCreate(ID_TEXT5, 20, 40, 75, 70,DRAW|ST_CENTER_ALIGN,"-1400",               alt4Scheme);
      BtnCreate(ID_BUTUP3,  10, 70, 50,110,10,BTN_DRAW,           NULL,"Up",      alt3Scheme);
      BtnCreate(ID_BUTDN3,  60, 70,100,110,10,BTN_DRAW,           NULL,"Dn",      alt3Scheme);

      StCreate(ID_TEXT7, 20,150, 75,180,DRAW|ST_CENTER_ALIGN,"+2550",               alt4Scheme);
      BtnCreate(ID_BUTUP5,  10,180, 50,220,10,BTN_DRAW,           NULL,"Up",      alt3Scheme);
      BtnCreate(ID_BUTDN5,  60,180,100,220,10,BTN_DRAW,           NULL,"Dn",      alt3Scheme);

      StCreate(ID_TEXT8,     210, 40,300,60,    ST_DRAW|ST_CENTER_ALIGN, "7",       alt4Scheme);
      BtnCreate(ID_BUTUP4,   210, 70,250,110,10,BTN_DRAW,           NULL,"Up",      alt3Scheme);
      BtnCreate(ID_BUTDN4,   260, 70,300,110,10,BTN_DRAW,           NULL,"Dn",      alt3Scheme);
      BtnCreate(ID_BUTDN6,   220,135,300,170,10,BTN_DRAW|BTN_TOGGLE,NULL,"NR",      alt3Scheme);

      BtnCreate(ID_BUTOK,   220,180,300,215,10,BTN_DRAW,            NULL,"OK",      alt3Scheme);

      break;

    case 4:     // - - - - - - Memories   W i n d o w - - - - - -

      if ( nMems & 0x01 )
	BtnCreate(ID_MEM1,  20, 10,180, 50,10,BTN_DRAW|BTN_TOGGLE|BTN_PRESSED,NULL,"14.060.000",      alt3Scheme);
      else
	BtnCreate(ID_MEM1,  20, 10,180, 50,10,BTN_DRAW|BTN_TOGGLE,NULL,"14.060.000",      alt3Scheme);

      if ( nMems & 0x02 )
	BtnCreate(ID_MEM2,  20, 55,180, 95,10,BTN_DRAW|BTN_TOGGLE|BTN_PRESSED,NULL,"14.058.000",      alt3Scheme);
      else
	BtnCreate(ID_MEM2,  20, 55,180, 95,10,BTN_DRAW|BTN_TOGGLE,NULL,"14.058.000",      alt3Scheme);

      if ( nMems & 0x04 )
	BtnCreate(ID_MEM3,  20,100,180,140,10,BTN_DRAW|BTN_TOGGLE|BTN_PRESSED,NULL,"14.070.000",      alt3Scheme);
      else
	BtnCreate(ID_MEM3,  20,100,180,140,10,BTN_DRAW|BTN_TOGGLE,NULL,"14.070.000",      alt3Scheme);

      if ( nMems & 0x08 )
	BtnCreate(ID_MEM4,  20,145,180,185,10,BTN_DRAW|BTN_TOGGLE|BTN_PRESSED,NULL,"14.040.000",      alt3Scheme);
      else
	BtnCreate(ID_MEM4,  20,145,180,185,10,BTN_DRAW|BTN_TOGGLE,NULL,"14.040.000",      alt3Scheme);

      if ( nMems & 0x10 )
	BtnCreate(ID_MEM5,  20,190,180,230,10,BTN_DRAW|BTN_TOGGLE|BTN_PRESSED,NULL,"14.020.000",      alt3Scheme);
      else
	BtnCreate(ID_MEM5,  20,190,180,230,10,BTN_DRAW|BTN_TOGGLE,NULL,"14.020.000",      alt3Scheme);

      BtnCreate(ID_BUTSTOR, 230,100,310,140,10,BTN_DRAW|BTN_TOGGLE,NULL,"Store",   alt3Scheme);
      BtnCreate(ID_BUTOK,   230,195,310,235,10,BTN_DRAW,           NULL,"OK",      alt3Scheme);

      break;

    default:
      break;
    }
  nNewPage = 0;

}
