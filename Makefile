CPP=g++
AR=ar
CFLAGS=
RM=rm -f
MAKE=make
DOC_DEPS=RPanel.config colors.h configbits.c Controls.h makePage.c \
	makeSchemes.c NumBFont.c NumFont.c ProgressBar.c RPanel.h \
	RPanel_main.c stringSubs.c mainTransitions.dot \
	msgCallbackFlow.dot msgCallbackFlow1.dot msgCallbackFlow2.dot \
        msgCallbackFlow4.dot AGC.dot Filter.dot

all : refman.pdf

tidy:
	$(RM) *.o
	$(RM) dox/html/*
	$(RM) dox/latex/*

clean:
	$(MAKE) tidy
	$(RM) refman.pdf
	$(RM) doxygen.log

startover:
	$(MAKE) tidy
	$(RM) refman.pdf
	$(RM) doxygen.log
	$(MAKE) all

refman.pdf : dox/latex/refman.pdf
	cp $< ./$@

dox/latex/refman.pdf : $(DOC_DEPS)
	doxygen RPanel.config
	make -C dox/latex
