/*! \file makeSchemes.c

    \brief Initialize color schemes for rPanel

    Function creates and initialise 4 schemes:
    \li wndScheme = basic panel background
    \li wnd2scheme = panel background with dimmed text
    \li alt4scheme = panel background with bright text
    \li alt2scheme = scheme for buttons

*/

#include "RPanel.h"
#include <string.h>
#include "colors.h"


// Color scheme for buttons
extern GOL_SCHEME*    alt3Scheme;        // alternative style scheme
// Background window emphasized text color scheme
extern GOL_SCHEME*    alt4Scheme;        // alternative style scheme
// Background window color scheme
extern GOL_SCHEME*    wndScheme;         // alternative style scheme
// Background window de-emphasized text color scheme
extern GOL_SCHEME*    wnd2Scheme;        // alternative style scheme
// Selected Scheme
extern int nScheme;
// Emphasized numbers font
extern FONT_FLASH Arial_BigNumbers;
// S Meter numbers font
extern FONT_FLASH Arial_Numbers;


//! makeSchemes - Build the color schemes
/*! Creates all the color schemes used on the various screens.
 */
void makeSchemes( void )
{

  alt3Scheme = GOLCreateScheme();     // create alternative 1 style scheme
  alt4Scheme = GOLCreateScheme();     // create alternative 1 style scheme
  wndScheme = GOLCreateScheme();      // create alternative 1 style scheme
  wnd2Scheme = GOLCreateScheme();     // create alternative 1 style scheme

  switch ( nScheme )
    {
    case 1:
      // Basic background scheme
      wndScheme->CommonBkColor = WHEAT;
      wndScheme->Color0 = wndScheme->CommonBkColor;
      wndScheme->Color1 = wndScheme->CommonBkColor;
      wndScheme->EmbossDkColor = DARKBLUEGRAY;
      wndScheme->EmbossLtColor = MEDIUMBLUEGRAY;
      wndScheme->ColorDisabled = BLUE1;
      wndScheme->TextColor1 = BLUE1;
      wndScheme->TextColor0 = SIENNA;
      wndScheme->TextColorDisabled = LIGHTGRAY1;

      // Faint number on wndScheme
      memcpy(wnd2Scheme,wndScheme,sizeof(GOL_SCHEME));
      wnd2Scheme->Color1 = PERU;
      wnd2Scheme->TextColor0 = TAN;
      // Small numbers for S meter
      wndScheme->pFont = &Arial_Numbers;

      // Buttons
      alt3Scheme->Color0 = SADDLEBROWN;       
      alt3Scheme->Color1 = SIENNA;
      alt3Scheme->TextColor0 = GOLD;
      alt3Scheme->TextColor1 =  BRIGHTYELLOW;
      alt3Scheme->EmbossDkColor = DARKSADDLEBROWN;
      alt3Scheme->EmbossLtColor = LIGHTPERU;

      // Emphasized text
      memcpy(alt4Scheme,wndScheme,sizeof(GOL_SCHEME));
      alt4Scheme->TextColor0 =  BLACK;
      alt4Scheme->EmbossDkColor = BLACK;
      alt4Scheme->EmbossLtColor = LIGHTERGRAY;
      // Bolder numbers for frequency, codespeed
      alt4Scheme->pFont = &Arial_BigNumbers;

      break;

    case 2:
      // Basic background scheme
      wndScheme->CommonBkColor = POWDERBLUE;
      wndScheme->Color0 = wndScheme->CommonBkColor;
      wndScheme->Color1 = wndScheme->CommonBkColor;
      wndScheme->EmbossDkColor = DARKBLUEGRAY;
      wndScheme->EmbossLtColor = MEDIUMBLUEGRAY;
      wndScheme->ColorDisabled = BRIGHTMAGENTA;
      wndScheme->TextColor1 = RED;
      wndScheme->TextColor0 = CADETBLUE;
      wndScheme->TextColorDisabled = HOTPINK;

      // Faint number on wndScheme
      memcpy(wnd2Scheme,wndScheme,sizeof(GOL_SCHEME));
      wnd2Scheme->Color1 = BRIGHTYELLOW;
      wnd2Scheme->TextColor0 = LIGHTBLUE;

      wndScheme->pFont = &Arial_Numbers;

      // Buttons
      alt3Scheme->Color0 = GRAY4;         
      alt3Scheme->Color1 = GRAY3;
      alt3Scheme->TextColor0 = POWDERBLUE;
      alt3Scheme->TextColor1 =  LIGHTCYAN;
      alt3Scheme->EmbossDkColor = GRAY2;
      alt3Scheme->EmbossLtColor = GRAY5;

      // Emphasized text
      memcpy(alt4Scheme,wndScheme,sizeof(GOL_SCHEME));
      alt4Scheme->TextColor0 =  BLACK;
      alt4Scheme->EmbossDkColor = BLACK;
      alt4Scheme->EmbossLtColor = LIGHTERGRAY;
      alt4Scheme->pFont = &Arial_BigNumbers;
      break;

    case 3:
      // Basic background scheme
      wndScheme->CommonBkColor = BLACK;
      wndScheme->Color0 = wndScheme->CommonBkColor;
      wndScheme->Color1 = wndScheme->CommonBkColor;
      wndScheme->EmbossDkColor = DARKBLUEGRAY;
      wndScheme->EmbossLtColor = MEDIUMBLUEGRAY;
      wndScheme->ColorDisabled = BLUE1;
      wndScheme->TextColor1 = BLUE1;
      wndScheme->TextColor0 = YELLOWGREEN;
      wndScheme->TextColorDisabled = LIGHTGRAY1;

      // Dim number on wndScheme
      memcpy(wnd2Scheme,wndScheme,sizeof(GOL_SCHEME));
      wnd2Scheme->Color1 = BRIGHTRED;
      wnd2Scheme->TextColor0 = MEDIUMGREEN;

      wndScheme->pFont = &Arial_Numbers;

      // Buttons
      alt3Scheme->Color0 = DARKGRAY1;         
      alt3Scheme->Color1 = DARKERGRAY;
      alt3Scheme->TextColor0 = GREEN2;
      alt3Scheme->TextColor1 =  GREEN1;
      alt3Scheme->EmbossDkColor = GRAY6;
      alt3Scheme->EmbossLtColor = GRAY3;

      // Bright wheat text
      memcpy(alt4Scheme,wndScheme,sizeof(GOL_SCHEME));
      alt4Scheme->TextColor0 =  WHEAT;
      alt4Scheme->EmbossDkColor = BLACK;
      alt4Scheme->EmbossLtColor = LIGHTERGRAY;
      alt4Scheme->pFont = &Arial_BigNumbers;
      break;

    default:
      // Basic background scheme
      wndScheme->CommonBkColor = CADETBLUE;
      wndScheme->Color0 = wndScheme->CommonBkColor;
      wndScheme->Color1 = BRIGHTRED;
      wndScheme->EmbossDkColor = DARKBLUEGRAY;
      wndScheme->EmbossLtColor = MEDIUMBLUEGRAY;
      wndScheme->ColorDisabled = BLUE1;
      wndScheme->TextColor1 = BLUE1;
      wndScheme->TextColor0 = YELLOWGREEN;
      wndScheme->TextColorDisabled = LIGHTGRAY1;

      // Dim number on wndScheme
      memcpy(wnd2Scheme,wndScheme,sizeof(GOL_SCHEME));
      wnd2Scheme->CommonBkColor = POWDERBLUE;
      wnd2Scheme->Color0 = GRAY40;
      wnd2Scheme->Color1 = WHEAT;
      wnd2Scheme->TextColor0 = BLACK;
      wndScheme->TextColorDisabled = BRIGHTYELLOW;

      wndScheme->pFont = &Arial_Numbers;

      // Buttons
      alt3Scheme->Color0 = GRAY30;        
      alt3Scheme->Color1 = GRAY40;
      alt3Scheme->TextColor0 = GREEN2;
      alt3Scheme->TextColor1 =  GREEN1;
      alt3Scheme->EmbossDkColor = GRAY20;
      alt3Scheme->EmbossLtColor = GRAY50;

      // Yellow text
      memcpy(alt4Scheme,wndScheme,sizeof(GOL_SCHEME));
      alt4Scheme->Color1 = alt4Scheme->CommonBkColor;
      alt4Scheme->TextColor0 =  YELLOW1;
      alt4Scheme->EmbossDkColor = BLACK;
      alt4Scheme->EmbossLtColor = LIGHTERGRAY;
      alt4Scheme->pFont = &Arial_BigNumbers;
    }

}

